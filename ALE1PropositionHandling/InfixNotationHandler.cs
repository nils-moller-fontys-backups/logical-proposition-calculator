﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace ALE1PropositionHandling
{
    class InfixNotationHandler
    {
        //basically an associative array so i can use EscapeSequencesInfixNotation["~"] to get ¬, etc.
        private readonly NameValueCollection EscapeSequencesInfixNotation = new NameValueCollection();

        private string infixFormula;

        public InfixNotationHandler()
        {
            EscapeSequencesInfixNotation.Add("~", "\u00AC");
            EscapeSequencesInfixNotation.Add(">", "\u21D2");
            EscapeSequencesInfixNotation.Add("=", "\u21D4");
            EscapeSequencesInfixNotation.Add("&", "\u2227");
            EscapeSequencesInfixNotation.Add("|", "\u2228");
        }

        /// <summary>
        /// Returns the infix notation for the given formula.
        /// </summary>
        /// <param name="formula"></param>
        /// <returns></returns>
        public string ConvertAsciiToInfix(Formula formula)
        {
            infixFormula = string.Empty;
            HandleOperators(formula.RootNode);
            return infixFormula;
        }

        private void HandleOperators(Node currentNode)
        {
            if (FormulaNodeOptions.Operators.Contains(currentNode.Character))
            {
                infixFormula += "(";

                if (currentNode.Character != '~')
                {
                    if (currentNode.Left != null)
                    {
                        if (FormulaNodeOptions.Operators.Contains(currentNode.Left.Character)) //if the left is also an operator, do this all again
                        {
                            HandleOperators(currentNode.Left);
                        }
                        else //left is a letter
                        {
                            infixFormula += currentNode.Left.Character;
                        }
                    }

                    infixFormula += EscapeSequencesInfixNotation[currentNode.Character.ToString()];

                    if (currentNode.Right != null)
                    {
                        if (FormulaNodeOptions.Operators.Contains(currentNode.Right.Character)) //if the right is also an operator, do this all again
                        {
                            HandleOperators(currentNode.Right);
                        }
                        else //right is a letter
                        {
                            infixFormula += currentNode.Right.Character;
                        }
                    }
                }
                //for negation, add the operator first, then the left child. 
                //todo: There HAS to be a better way than custom implementation for a single operator.
                else
                {
                    infixFormula += EscapeSequencesInfixNotation[currentNode.Character.ToString()];

                    if (currentNode.Left != null)
                    {
                        if (FormulaNodeOptions.Operators.Contains(currentNode.Left.Character)) //if the left is also an operator, do this all again
                        {
                            HandleOperators(currentNode.Left);
                        }
                        else //left is a letter
                        {
                            infixFormula += currentNode.Left.Character;
                        }
                    }
                }
                

                infixFormula += ")";
            }
            //This needs to be here if the first character is not a formula. Otherwise nothing will show.
            else if (char.IsLetter(currentNode.Character))
            {
                infixFormula += currentNode.Character;
            }
        }
    }
}