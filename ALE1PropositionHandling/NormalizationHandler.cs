﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ALE1PropositionHandling
{
    /*
     * Steps to normalize a truth table:

      1.  Take all the true results.
      2.  For every row, apply a condition to the values of the variables to make them all true; 0s become "~(variable)", and 1s become "variable".
      3.  Make new formulas for every row with all the variables combining each of them with AND in either their negated form or their own form, as described in step 2.
      4.  Make one new formula combining all the formulas from step 3. with an OR operant.
     */
    class NormalizationHandler
    {
        public string Normalize(DataTable originalTruthTable)
        {
            DataTable intermediateTable = originalTruthTable.Copy();
            List<string> intermediateFormulas = new List<string>();
            string normalizedFormula = string.Empty;

            //calculation
            //remove false rows from the intermediate table
            for (int i = intermediateTable.Rows.Count - 1; i >= 0; i--) //go backwards because removing items while the loop is increasing disrupts the relative position i
            {
                if ((char)intermediateTable.Rows[i]["Result"] == '0')
                {
                    intermediateTable.Rows.RemoveAt(i);
                }
            }

            //now we have only true results

            //create the individual formulas from the rows
            foreach (DataRow row in intermediateTable.Rows)
            {
                string formula = string.Empty;
                int amountOfAnds = 0;

                for (int i = 0; i < intermediateTable.Columns.Count - 1; i++)
                {
                    if (i < intermediateTable.Columns.Count - 2)
                    {
                        formula += "&(";
                        amountOfAnds++;
                    }

                    if (row[i].Equals('0'))
                    {
                        formula += $"~({intermediateTable.Columns[i].ColumnName})";
                    }
                    else if (row[i].Equals('1'))
                    {
                        formula += intermediateTable.Columns[i].ColumnName;
                    }

                    if (i < intermediateTable.Columns.Count - 2)
                    {
                        formula += ",";
                    }
                }

                //close off all the ands
                for (int i = 0; i < amountOfAnds; i++)
                {
                    formula += ")";
                }

                intermediateFormulas.Add(formula);
            }

            //add all the formulas together to make one normalized formula
            int amountOfOrs = 0;
            for (int i = 0; i < intermediateFormulas.Count; i++)
            {
                if (i != intermediateFormulas.Count - 1)
                {
                    normalizedFormula += "|(";
                    amountOfOrs++;
                }

                normalizedFormula += intermediateFormulas[i];

                if (i != intermediateFormulas.Count - 1)
                {
                    normalizedFormula += ",";
                }
            }

            //close off all the ors
            for (int i = 0; i < amountOfOrs; i++)
            {
                normalizedFormula += ")";
            }

            return normalizedFormula;
        }
    }
        
}
