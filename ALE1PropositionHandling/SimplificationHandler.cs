﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ALE1PropositionHandling
{
    /*
     * only take results 1 in consideration, we dont care about falses. Falses we just copy paste in the new table 
     * if not able to simplify, just put the row in the new table
     * 
     * go step by step, do everything with one variable as *, then go to the next step of simplifying 2 variables
     * Can use row as many times as you want for simplifying (use row 0 and row 1 and use row 0 and row 2)
     * Use simplification table 1 (only 1 variable as *) to calculate 2 simplified variables, go back and double check of the result is true
     * If all results are 1, all variables are *. If all results are 0, no variables are * CHECK THIS FIRST (check hex?)
     * idea: number the rows, for every row, compare to all other rows and check if can simplify (use datatables, for in for)
     * 
     * if wanna optimize: think about how they are combined in binary. if you have A, B, C. Check with the most * variables (big to small) to ignore ones you already have used. When you check for B.
     * Use only binary, from big to small. Try 4, then 2, then 1.
     * 
     * Quine-McCluskey algorithm (https://www.youtube.com/watch?v=VnZLRrJYa2I):
     * groups rows into groups of how many 1s (group with all 0s, group with one 1 in the binary number, group with 2 1s in the binary, group with 3 1s in the binary
     * check each of group 0 with each of group 1, each of group 1 with each of group 2, etc. Must only differ one cell (0001 with 0101 is possible, but 0001 with 1101 is not because there are 2 cells that are different. Put a check mark next to the indexes you've used. 
     * If all are checked you dont have to look back at that column, the answer doesnt change
     * the ones without a check mark cannot be simplified anymore and will be in the final simplified table
     */
    class SimplificationHandler
    {
        /// <summary>
        /// Uses the truth table to generate and return the simplified table.
        /// </summary>
        /// <param name="originalTruthTable">The truth table to be simplified.</param>
        /// <returns>Returns the simplified truth table.</returns>
        public DataTable GetSimplifiedTruthTable(DataTable originalTruthTable)
        {
            //add columns to simplified table
            DataTable intermediateTable = originalTruthTable.Copy(); //the table to hold one iteration of simplification
            DataTable simplifiedTable = originalTruthTable.Clone(); //the last simplified table
            DataTable zeroResults = originalTruthTable.Clone();

            //check for tautology
            int[] results = new int[originalTruthTable.Rows.Count];
            for (int i = 0; i < originalTruthTable.Rows.Count; i++)
            {
                results[i] = ((int)char.GetNumericValue((char)originalTruthTable.Rows[i]["Result"]));
            }

            if (!results.Contains(1)) //return original table because we can't simplify false results
            {
                return originalTruthTable;
            }
            else if (!results.Contains(0)) //in case of all 1s the simplification table has 1 row with all *s and 1 for a result
            {
                DataRow row = simplifiedTable.NewRow();

                for (int i = 0; i < simplifiedTable.Columns.Count - 1; i++) //set all the variables to *
                {
                    row[i] = '*';
                }
                simplifiedTable.Rows.Add(row);

                row["Result"] = '1';
                return simplifiedTable;
            }

            //calculation
            //add false rows to the simplified table and remove them from the intermediate one
            for (int i = intermediateTable.Rows.Count - 1; i >= 0; i--) //go backwards because removing items while the loop is increasing disrupts the relative position i
            {
                if ((char)intermediateTable.Rows[i]["Result"] == '0')
                {
                    zeroResults.ImportRow(intermediateTable.Rows[i]);
                    intermediateTable.Rows.RemoveAt(i);
                }
            }

            simplifiedTable = Simplify(AssignRowsToGroups(intermediateTable), simplifiedTable, intermediateTable);

            simplifiedTable.Merge(zeroResults);

            return simplifiedTable;
        }

        /// <summary>
        /// Uses the groups to simplify and populate the given table.
        /// </summary>
        /// <param name="groups">The groups to simplify.</param>
        /// <param name="newTable">The initialized DataTable to be populated.</param>
        /// <param name="previousTable">The table to check against when checking for the recursive end.</param>
        /// <returns></returns>
        private DataTable Simplify(Dictionary<string, List<DataRow>> groups, DataTable newTable, DataTable previousTable)
        {
            /*
            * i is current group index
            * i + 1 is next group index
            * j is current row index
            * k is the next group row index
            * l is the current column index
            */

            //check if this is the last simplification before tautology by checking the amount of stars in a row. If the amount of stars is the same as amount of variables - 1 we cant simplify more and we just return the previousTable
            for (int i = 0; i < previousTable.Rows.Count; i++)
            {
                DataRow currentRow = previousTable.Rows[i];
                //check for differences
                int amountOfStars = 0;
                for (int j = 0; j < newTable.Columns.Count - 1; j++)
                {
                    //add the column indexes of the variables that differ
                    if (currentRow[j].Equals('*'))
                    {
                        amountOfStars++;
                    }
                }
                if (amountOfStars == previousTable.Columns.Count - 2) //-2 because we dont want the result row taken into account, and we are looking for one star less than the amount of variables
                {
                    return previousTable;
                }
            }

            List<DataRow> nextGroupUsedRows = new List<DataRow>();

            //for every group apart from the last one
            for (int i = 0; i < groups.Count - 1; i++)
            {
                List<DataRow> currentGroup = groups.ElementAt(i).Value;
                List<DataRow> currentGroupUsedRows = nextGroupUsedRows;
                nextGroupUsedRows = new List<DataRow>();

                //for every row in the current group
                for (int j = 0; j < currentGroup.Count; j++)
                {
                    DataRow currentRow = currentGroup[j];

                    //for every row in the next group
                    for (int k = 0; k < groups.ElementAt(i + 1).Value.Count; k++)
                    {
                        DataRow nextGroupCurrentRow = groups.ElementAt(i + 1).Value[k];
                        List<int> differentValueIndexes = new List<int>();

                        //check for differences
                        for (int l = 0; l < newTable.Columns.Count - 1; l++)
                        {
                            //add the column indexes of the variables that differ
                            if (!currentRow.ItemArray[l].Equals(nextGroupCurrentRow.ItemArray[l]))
                            {
                                differentValueIndexes.Add(l);
                            }
                        }

                        //if there is only one different value, we can simplify
                        if (differentValueIndexes.Count == 1)
                        {
                            DataRow newRow = newTable.NewRow();
                            newRow.ItemArray = currentRow.ItemArray; //copy the values from the current row that we're checking
                            newRow.SetField(newTable.Columns[differentValueIndexes[0]], '*');
                            newTable.Rows.Add(newRow);
                            newTable = newTable.DefaultView.ToTable(true);

                            if (!currentGroupUsedRows.Contains(currentRow))
                                currentGroupUsedRows.Add(currentRow);
                            if (!nextGroupUsedRows.Contains(nextGroupCurrentRow))
                                nextGroupUsedRows.Add(nextGroupCurrentRow);
                        }
                    }
                }

                //once its finished processing, remove the used rows from the current group
                foreach (DataRow row in currentGroupUsedRows)
                {
                    currentGroup.Remove(row);
                }

                //add the rows that were not used and remove it from the groups
                for (int j = 0; j < currentGroup.Count; j++)
                {
                    newTable.ImportRow(currentGroup.ElementAt(j));
                    currentGroup.RemoveAt(j);
                }
            }

            //add the rows that were not used from the last group
            foreach (DataRow row in groups.Last().Value)
            {
                if (!nextGroupUsedRows.Contains(row))
                {
                    newTable.ImportRow(row);
                }
            }

            //if we do have a previous table, check if they are the same
            List<DataRow> differences = previousTable.AsEnumerable().Except(newTable.AsEnumerable(), DataRowComparer.Default).ToList();

            //if there are differences, run the simplification again
            if (differences.Any() && newTable.Rows.Count > 0)
            {
                return Simplify(AssignRowsToGroups(newTable), newTable.Clone(), newTable);
            }
            //if we have no differences, it means that the last run produced the same result as the current run and more simplification is not possible. Return the table
            else
            {
                return newTable;
            }
        }

        /// <summary>
        /// Groups the DataRows based on the amount of ones in their binary row index. <br></br>
        /// The key for each group is made using the amount of ones: 0Ones, 1Ones, 2Ones, etc.
        /// </summary>
        /// <param name="dataToGroup">The DataTable to organize in groups.</param>
        /// <returns>Returns a dictionary with the groups assigned into the amount of stars in the row.</returns>
        private Dictionary<string, List<DataRow>> AssignRowsToGroups(DataTable dataToGroup)
        {
            Dictionary<string, List<DataRow>> groups = new Dictionary<string, List<DataRow>>();

            for (int i = 0; i < dataToGroup.Columns.Count; i++) //initiate the groups so they are in the right order
            {
                groups.Add($"{i}Ones", new List<DataRow>());
            }

            foreach (DataRow row in dataToGroup.Rows)
            {
                int amountOfOnes = 0;
                for (int i = 0; i < dataToGroup.Columns.Count - 1; i++) //-1 because we dont want to look at the result column
                {
                    if ((int)char.GetNumericValue((char)row[i]) == 1) //this check is flawed. If it finds 0 1 it adds it to a group but if it finds 1 0 it doesnt add the second entry for 1 0
                    {
                        amountOfOnes++;
                    }
                }
                groups[$"{amountOfOnes}Ones"].Add(row);
            }

            Dictionary<string, List<DataRow>> temp = new Dictionary<string, List<DataRow>>();

            for (int i = 0; i < groups.Count; i++)
            {
                if (groups.ElementAt(i).Value.Count > 0)
                {
                    temp.Add(groups.ElementAt(i).Key, groups.ElementAt(i).Value);
                }
            }

            return temp;
        }
    }
}
